# README #

### What is this repository for? ###

This repo is originally for scraping data from the EWG Tapwater website, but the code can be readily translated to scraping any other site.

### How do I get set up? ###

First, review Scrapy:
* https://docs.scrapy.org/en/latest/intro/tutorial.html

Scrapy can be really finicky. The folder structure for scrapy spiders cannot be modified in any way.
All spiders should be run in a virtual env:
* https://virtualenv.pypa.io/en/stable/userguide/