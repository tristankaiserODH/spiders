import scrapy
import csv
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
import numpy as np

with open("environment/spiders/Data/compliance_links_full.txt") as f:
    compliance_links = f.read().splitlines()


class EwgWaterComplianceSpider(CrawlSpider):
    name = "ewg_water_compliance"
    allowed_domains = ["ewg.com"]

    start_urls = [
        "https://www.ewg.org/tapwater/{}".format(i) for i in compliance_links
#        "https://www.ewg.org/tapwater/system.php?pws=MA1159000",
#        "https://www.ewg.org/tapwater/system.php?pws=VA6153050"
    ]

    def parse(self, response):
        for sel in response.xpath('//html'):
            yield{
                'city': sel.xpath('normalize-space(//ul[@class="served-ul"]/li[1]/h2/text())').extract_first(),
                'population': sel.xpath('normalize-space(//ul[@class="served-ul"]/li[2]/h2/text())').extract_first(),
                'dates': sel.xpath('normalize-space(//ul[@class="served-ul"]/li[3]/h2/text())').extract_first(),
                'source': sel.xpath('normalize-space(//ul[@class="served-ul"]/li[4]/h2/text())').extract_first(),
                'utility': sel.xpath('normalize-space(//main//h1/text())').extract_first(),
                'contaminants_above': sel.xpath('normalize-space(//div[@id="contaminant-tile-above-hbl"][1]/p[@class="contaminant-tile-number"]/text())').extract_first(),
                'other_contaminants': sel.xpath('normalize-space(//div[@id="contaminant-tile-other"][1]/p[@class="contaminant-tile-number"]/text())').extract_first(),
                'violation_1': sel.xpath('normalize-space(//section[@class="water-utility-compliance"]//li[1]/div[@class="compliance-btn"]/text())').extract_first(),
                'violation_2': sel.xpath('normalize-space(//section[@class="water-utility-compliance"]//li[2]/div[@class="compliance-btn"]/text())').extract_first(),
                'violation_3': sel.xpath('normalize-space(//section[@class="water-utility-compliance"]//li[3]/div[@class="compliance-btn"]/text())').extract_first(),
                'violation_text_1': sel.xpath('normalize-space(//section[@class="water-utility-compliance"]//li[1]/text())').extract_first(),
                'violation_text_2': sel.xpath('normalize-space(//section[@class="water-utility-compliance"]//li[2]/text())').extract_first(),
                'violation_text_3': sel.xpath('normalize-space(//section[@class="water-utility-compliance"]//li[3]/text())').extract_first(),
            }

