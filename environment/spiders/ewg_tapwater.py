import scrapy
import csv
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
import numpy as np

# This script gathers all utility links from the EWG site based on the zipcodes
# provided by zips.txt
with open("environment/spiders/Data/zips_full.txt") as f:
    zips = f.read().splitlines()


class EwgWaterSpider(CrawlSpider):
    name = "ewg_water_links"
    allowed_domains = ["ewg.com"]

    start_urls = [
        "https://www.ewg.org/tapwater/search-results.php?zip5={}".format(i) for i in zips
    ]

    def parse(self, response):
        for sel in response.xpath('//html'):
            yield{
                'link': sel.xpath('//tbody//tr//td/a/@href').extract(),
            }